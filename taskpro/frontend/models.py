import hashlib
import uuid
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import validate_email

class Profile(User):
    """
    This is genric profile used to store info regarding various users and their types
    """
    GENDER_CHOICES = (
        ('Male','Male'),
        ('Female','Female'),
        )
    token = models.CharField(max_length=255, blank=True, default='')
    secret = models.CharField(max_length=255, blank=True, default='')
    gender = models.CharField(max_length=50, choices=GENDER_CHOICES)
    qualification = models.CharField(max_length=15, blank=True, default='')
    address = models.CharField(max_length=100, blank=True, default='')
    city = models.CharField(max_length=15, blank=True, default='')
    mobile = models.CharField(max_length=15, blank=True, default='')
    dob = models.DateField(max_length=15,blank=True)

    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)

    @classmethod
    def gen_token_secret(self, user_id):
        """
        This method is used to generate token and secret
        """
        profile = Profile.objects.get(id=user_id)
        profile.token = hashlib.md5(profile.username).hexdigest()
        profile.secret = hashlib.md5(profile.password + str(datetime.now())).hexdigest()
        profile.save()
        return profile.token, profile.secret

    @classmethod
    def get_profile_info(self, user_id, INCLUDE_CREDS=False):
        """
        This method is used to return profile related information
        """
        data = {}
        try:
            profile = Profile.objects.get(id=user_id)
        except:
            return data

        data['profile_id'] = profile.id
        data['username'] = profile.username
        data['email'] = profile.email
        data['name'] = profile.first_name + " " + profile.last_name
        data['firstname'] = profile.first_name
        data['lastname'] = profile.last_name
        data['gender'] = profile.gender
        data['address'] = profile.address
        data['landline'] = profile.landline
        data['mobile'] = profile.mobile
        data['qualification'] = profile.qualification

        if INCLUDE_CREDS:
            token = profile.token
            secret = profile.secret

            if profile.token.strip() == "" or profile.secret.strip() == "":
                token, secret = self.gen_token_secret(user_id)

            data['token'] = token
            data['secret'] = secret

        return data

    @classmethod
    def validate_token_secret(self, token, secret):
        """
        This method is used to validate token secret
        """
        try:
            profile = Profile.objects.get(token=token)
            return profile.secret == secret
        except:
            return False

    @classmethod
    def validate_username_passwd(self, username, passwd):
        """
        This method is used to validate username and password
        """
        try:
            profile = Profile.objects.get(username=username)
            return profile.check_password(passwd)
        except:
            return False


    @classmethod
    def exists(self, value, email=False):
        """
        This method is used to check if email/username exists
        """
        if email:
            return Profile.objects.filter(email=value).count() != 0
        else:
            return Profile.objects.filter(username=value).count() != 0

    @classmethod
    def id_exists(self, profile_id):
        """
        This method is used to check if profile_id's object exists in our db
        """
        try:
            return Profile.objects.filter(id=profile_id).count() == 1
        except:
            return False

    @classmethod
    def validate_email(self,email):
        try:
            validate_email(email)
            return True
        except:
            return False


class APIError:
    """
    This class is used to handle errors at API level and return associated error codes
    """
    @classmethod
    def raise_invalid_input(self):
        """
        This method is used to return JSON for invalid input case
        """
        data = {"status": 0,
                "msg": "Invalid input params. Please check and retry"}
        return data

    @classmethod
    def raise_auth_failed(self):
        """
        This method is used to return JSON for invalid username and password
        """
        data = {"status": -1,
                "msg": "Authentication failed"}
        return data

    @classmethod
    def raise_email_or_username_exists(self):
        """
        Used to raise error when email or username exists
        """
        data = {"status": -2,
                "msg": "Email or Username already exists"}
        return data

    @classmethod
    def raise_email_or_username_doesnt_exists(self):
        """
        Used to raise error when email or username does not exists
        """
        data = {"status": -3,
                "msg": "Username doesn't exists"}
        return data


    @classmethod
    def raise_invalid_email(self):
        data = {"status": -4,
                "msg": "Enter a valid Email address"}
        return data



class WebappError:
    """
    This class is used to handle errors at webapp and return associated errors
    """
    @classmethod
    def raise_email_exists(self):
        """
        raises error when email already exists
        """
        msg = "Email already exists."
        return msg

    @classmethod
    def raise_email_or_username_exists(self):
        """
        Used to raise error when email or username exists
        """
        msg = "Email or Username already exists"
        return msg

    @classmethod
    def raise_invalid_dob(self):
        """
        used to raise error when date is provided in invalid format
        """
        msg = "Invalid date."

    @classmethod
    def raise_auth_failed(self):
        """
        This method is used to return JSON for invalid username and password
        """
        msg = "Authentication failed"
        return msg

    @classmethod
    def raise_password_match_failed(self):
        """
        This method is called if entered password do not match with db password
        """
        msg = "Authentication failed. Enter your old password again."
        return msg


class Task(models.Model):
    """
    This class is used to handle task
    """
    
    TASK_ASSIGN_STATUS_CHOICES = (('Not Assigned', 'Not Assigned'),
                                  ('Assigned', 'Assigned'))

    TASK_APPROVAL_STATUS_CHOICES = (('Pending Approval','Pending Approval'),
                                    ('Approved', 'Approved'),
                                    ('Rejected', 'Rejected'))

    TASK_COMPLETION_STATUS_CHOOICES = (('Started', 'Started'),
                                       ('In Progress', 'In Progress'),
                                       ('Completed', 'Completed'))

    user = models.ForeignKey(Profile, unique=True, blank=True, null=True, related_name="Assign Task")
    title = models.CharField(max_length=150, default='')
    description = models.TextField(null=True, blank=True, default=None)
    location = models.CharField(max_length=150, default='')
    assign_status = models.CharField(max_length=100, choices=TASK_ASSIGN_STATUS_CHOICES, default='Not Assigned')
    approval_status = models.CharField(max_length=100, blank=True,choices=TASK_APPROVAL_STATUS_CHOICES)
    completion_status = models.CharField(max_length=100, blank=True,choices=TASK_COMPLETION_STATUS_CHOOICES)
    application = models.TextField(null=True, blank=True, default=None)
    objective = models.TextField(null=True, blank=True, default=None)








