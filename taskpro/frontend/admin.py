from django.contrib import admin
from frontend.models import *

admin.site.register(Profile)

class TaskAdmin(admin.ModelAdmin):
    list_display = ('title','assign_status','approval_status','completion_status')

admin.site.register(Task,TaskAdmin)
