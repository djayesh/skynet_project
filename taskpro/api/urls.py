from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('api.views',
    url(r'^login$',                 'login'),

    url(r'user/signup',             'signup'),
    url(r'user/edit$',              'edit_user'),


    url(r'^admin/', include(admin.site.urls)),
)