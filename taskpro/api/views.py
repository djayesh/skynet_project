import json
from django.views.decorators.csrf import csrf_exempt
from frontend.models import *
from django.http import HttpResponse

@csrf_exempt
def login(request):
    """
    API method to login users
    """
    username = request.POST.get('username', '')
    passwd = request.POST.get('passwd', '')
    response = {'response': {'status': 0}}

    if username.strip() == "" or passwd.strip() == "":
        response['response'] = APIError.raise_invalid_input()

    elif not Profile.validate_username_passwd(username, passwd):
        response['response'] = APIError.raise_auth_failed()

    else:
        user_id = Profile.objects.get(username=username).id
        profile = Profile.get_profile_info(user_id, True)

        request.session['token'] = profile['token']
        request.session['secret'] = profile['secret']
        request.session['username'] = profile['username']
        request.session.set_expiry(100000)

        response['response']["status"] = 1
        response['response']["msg"] = "Login sucessfully"
        response['response']['credentials'] = profile

    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def signup(request):
    """
    API method for signup users
    """
    print "into sign up"
    username = (request.POST.get('username', '')).lower()
    first_name = request.POST.get('first_name', '')
    last_name = request.POST.get('last_name', '')
    email = request.POST.get('email', '')
    gender =request.POST.get('gender','')
    passwd = request.POST.get('passwd', '')
    address = request.POST.get('address', '')
    city = request.POST.get('city', '')
    mobile = request.POST.get('mobile', '')
    qualification = request.POST.get('qualification', '')
    dob= request.POST.get('dob','')


    response = {'response': {'status': 0}}

    if username.strip() == "" or first_name.strip() == "" or last_name.strip() == "" or email.strip() == "" or passwd.strip() == "":
        response['response'] = APIError.raise_invalid_input()

    elif not Profile.validate_email(email):
        response['response'] = APIError.raise_invalid_email()

    elif Profile.exists(username) or Profile.exists(email, True):
        response['response'] = APIError.raise_email_or_username_exists()

    else:
        profile = Profile()
        profile.username = username
        profile.first_name = first_name.capitalize()
        profile.last_name = last_name.capitalize()
        profile.email = email
        profile.address = address
        profile.city = city
        profile.mobile = mobile
        profile.qualification = qualification
        profile.gender = gender
        profile.dob = dob
        profile.set_password(passwd)
        profile.save()

        profile = Profile.get_profile_info(profile.id)
        response['response']["status"] = 1
        response['response']["msg"] = "Added Profile sucessfully"
        response['response']['credentials'] = profile

    return HttpResponse(json.dumps(response), content_type="application/json")

@csrf_exempt
def edit_user(request):
    """
    API method for edit users
    """

    token = request.POST.get('token', '')
    secret = request.POST.get('secret', '')
    username = (request.POST.get('username', '')).lower()
    first_name = request.POST.get('first_name', '')
    last_name = request.POST.get('last_name', '')
    email = request.POST.get('email', '')
    gender =request.POST.get('gender','')
    passwd = request.POST.get('passwd', '')
    address = request.POST.get('address', '')
    landline = request.POST.get('landline', '')
    mobile = request.POST.get('mobile', '')
    qualification = request.POST.get('qualification', '')

    response = {'response': {'status': 0}}

    if username.strip() == "" or first_name.strip() == "" or last_name.strip() == "" or email.strip() == "":
        response['response'] = APIError.raise_invalid_input()

    elif not Profile.validate_token_secret(token, secret):
        response['response'] = APIError.raise_auth_failed()

    elif not Profile.validate_email(email):
        response['response'] = APIError.raise_invalid_email()

    elif not Profile.exists(username):
        response['response'] = APIError.raise_email_or_username_doesnt_exists()

    elif Profile.objects.filter(~Q(username=username),email=email):
        response['response'] = APIError.raise_email_or_username_exists()

    else:
        profile = Profile.objects.get(username=username)
        profile.first_name = first_name.capitalize()
        profile.last_name = last_name.capitalize()
        profile.email = email
        profile.address = address
        profile.landline = landline
        profile.mobile = mobile
        profile.qualification = qualification
        profile.gender = gender
        profile.save()

        profile = Profile.get_profile_info(profile.id)
        response['response']["status"] = 1
        response['response']["msg"] = "Updated User info sucessfully"
        response['response']['credentials'] = profile

    return HttpResponse(json.dumps(response), content_type="application/json")

